import Sequelize from 'sequelize';
import md5 from 'md5';
import jwt from 'jsonwebtoken'
import config from '../config.json';

export default (db) => {

    const User = db.define('user', {
        username: {
            type: Sequelize.STRING,
            unique: true,
            lowercase: true,
            required: true,
            trim: true,
        },
        firstname: {
            type: Sequelize.STRING,
            required: true
        },
        lastname: {
            type: Sequelize.STRING,
            required: true
        },
        email: {
            type: Sequelize.STRING,
            required: true,
        },

        pass: {
            type: Sequelize.STRING,
            required: true,
        }
    });
    User.register = async function (user_details, res) {
        try {
            let registeration_result = await User.create({
                username: user_details.username,
                firstname: user_details.firstname,
                lastname: user_details.lastname,
                email: user_details.email,
                pass: md5(user_details.pass),
            })
            return registeration_result;
        } catch (err) {
            throw new Error(err.errors[0].message);
        }
    };

    User.login = async function (login_details, res) {

        try {
            let login_res = await User.findOne({
                where: {
                    username: login_details.username,
                    pass: md5(login_details.pass)
                }
            })
            if (login_res != null) {
                const payload = {
                    admin: login_res.username
                };
                var token = jwt.sign(payload, config.secret, {
                    expiresIn: config.expiryTime,
                });

                let data = {
                    message: 'Login Successfully done..... Enjoy your token!',
                    token: token
                }
                return data;
            } else {
                throw new Error("Authentication failed::Check your Username Password")
            }
        } catch (err) {
            throw new Error(err.message);
        }
    };

    User.get = async function (req, res) {
        try {
            let fetch_res = await User.find({
                where: {
                    username: req.username
                },
                attributes: ['username', 'firstname', 'lastname', 'email']
            })
            if (fetch_res != null) {
                return fetch_res;
            } else {
                throw new Error("Authentication failed::Check your Username Password")
            }
        } catch (err) {
            throw new Error(err.message);
        }
    };

    User.delete = async function (req, res) {
        try {
            let delete_res = await User.destroy({
                where: {
                    username: req.username
                }
            })
            if (delete_res != null) {
                return "User ` " + req.username + " ` data has been deleted"
            } else {
                throw new Error("Something Wrong :: Unable to delete the data od the", req.username)
            }
        } catch (err) {
            throw new Error(err.message);
        }
    };
    return User;
};