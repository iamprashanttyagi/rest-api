import Sequelize from 'sequelize';
import config from './config.json';

import models from "./models";
const db = {};
export default callback => {
	let sequelize = new Sequelize(config.db_name, config.username, config.password, {
		host: config.host,
		dialect: config.dialect,
		operatorsAliases: config.alias,

	});

	// for load the models
	Object.keys(models).forEach((modelName) => {
		const model = models[modelName](sequelize, Sequelize.DataTypes);
		db[modelName] = model;
	});

	// associations on each of the models
	Object.keys(db).forEach((modelName) => {
		if (db[modelName].options.associate) {
			db[modelName].options.associate(db);
		}
	});

	sequelize.sync();
	callback(sequelize);
}