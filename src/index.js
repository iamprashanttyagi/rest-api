import http from 'http';
import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import initializeDb from './db';
import middleware from './middleware';
import api from './api/user';
import config from './config.json';
import validator from 'express-validator';
let app = express();
app.server = http.createServer(app);

// logger
app.use(morgan('dev'));

app.use(bodyParser.json({
	limit: config.bodyLimit
}));

app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(validator());

app.use(express.urlencoded({
  extended: false
}));


// connect to db
initializeDb( db => {
	// internal middleware
	app.use(middleware({config, db}));

	// api router
	app.use('/user', api({ config, db }));

	app.server.listen(process.env.PORT || config.port, () => {
		console.log(`Started on port ${app.server.address().port}`);
	});
});

export default app;