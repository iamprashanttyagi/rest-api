import BaseAPIController from './BaseAPIController';
import UserProvider from "../providers/UserProvider.js";
import User_model from '../models/User.js'
import pushMessageService from '../services/pushMessage';

export class User extends BaseAPIController {

    /* Controller for User Register  */
    register = async (req, res, next, db) => {
        try {
            let valid_data = await UserProvider.register(req.checkBody, req.validationErrors);
            let resgister_result = await User_model(db).register(req.body, res);
            this.handleSuccessResponse(req, res, next, resgister_result)
        } catch (err) {
            this.handleErrorResponse(res, err)
        }
    }
    login = async (req, res, next, db) => {
        try {
            let valid_data = await UserProvider.login(req, res);
            let login_result = await User_model(db).login(req.body, res);
            this.handleSuccessResponse(req, res, next, login_result)
        } catch (err) {
            this.handleErrorResponse(res, err)
        }
    }
    get = async (req, res, next, db) => {
        try {
            let fetch_result = await User_model(db).get(req, res);
            this.handleSuccessResponse(req, res, next, fetch_result)
        } catch (err) {
            this.handleErrorResponse(res, err)
        }
    }
    delete = async (req, res, next, db) => {
        try {
            let delete_result = await User_model(db).delete(req, res);
            this.handleSuccessResponse(req, res, next, delete_result)
        } catch (err) {
            this.handleErrorResponse(res, err)
        }
    }
    notify = async (req, res, next, config) => {
        try {
            let notify_res = await pushMessageService.pushMessage(config);
            this.handleSuccessResponse(req, res, next, notify_res)
        } catch (err) {
            this.handleErrorResponse(res, err)
        }
    }
}
const controller = new User();
export default controller;