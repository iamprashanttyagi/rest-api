import {Router} from 'express';
import user from '../controllers/user';
import auth from "../middleware/auth";

export default ({ config, db }) => {
	let api = Router();

	api.post('/register',  (req, res, next)=>{user.register(req, res, next, db)});
	api.post('/login',  (req, res, next)=>{user.login(req, res, next, db)});
	api.get('/get', auth.verifyToken , (req, res, next)=>{user.get(req, res, next, db)});
	api.put('/delete', auth.verifyToken , (req, res, next)=>{user.delete(req, res, next, db)});
	api.get('/notify', (req, res, next)=>{user.notify(req, res, next, config)})
	return api;
}
