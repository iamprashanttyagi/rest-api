import jwt from 'jsonwebtoken';
import config from '../config.json';
export class AuthController {
    verifyToken(req, res, next) {
        var token = req.headers.token || req.query.token || req.headers['x-access-token'];
        if (token) {
            jwt.verify(token, config.secret, function (err, decoded) {
                if (err) {
                    next(res.status(400).send({
                        status: false,
                        message: "Wrong Token"
                    }));
                } else {
                    req.username = decoded.admin
                    next();
                }
            });
        } else {
            next(res.status(400).send({
                message: "Token Not Found"
            }));
        }
    }
}

const controller = new AuthController();
export default controller;