import FCM from 'fcm-node';

let pushMessage = async (config) => {
    try {
        const serverKey = config.serverKey;

        const fcm = new FCM(serverKey);

        const message = {
            to: config.deviceId, //here provide the device id
            notification: {
                title: config.title, // title of the notification
                body: config.body, // Body of the notification
                click_action: config.clickAction,
            },

            data: {
                product_id: config.productId, //example for data for some kind of the id
            },
        };

        fcm.send(message, (err, response));
        return 'Successfully sent with response: ', response;
    } catch (err) {
        throw new Error('Something has gone wrong!', err)
    }
}

export default {
    pushMessage
}